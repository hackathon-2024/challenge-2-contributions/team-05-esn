# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from search import retrieve_texts
import pandas as pd
from src.utils import get_collection
import argparse

parser = argparse.ArgumentParser(description="Process questions and generate answers.")
parser.add_argument(
    "--questions-csv", type=str, help="Path to the questions CSV file", required=True
)
parser.add_argument(
    "--output-dir", type=str, help="Path to the output directory", required=True
)
args = parser.parse_args()

# defer loading of transformers until after the arg parsing
from transformers import AutoModelForCausalLM, AutoTokenizer  # noqa: E402

# Load questions.csv
collection = get_collection()
questions_df = pd.read_csv(args.questions_csv)
questions = questions_df["questions"]

MODEL_ID = "mistralai/Mixtral-8x7B-Instruct-v0.1"
tokenizer = AutoTokenizer.from_pretrained(MODEL_ID)
model = AutoModelForCausalLM.from_pretrained(MODEL_ID)


for question in questions:
    print(question)

    qn_id = question.split(";")[0]
    documents = retrieve_texts(question)
    question += " answer this question using these documents" + documents
    inputs = tokenizer(question, return_tensors="pt")
    outputs = model.generate(**inputs, max_new_tokens=20)

    with open(f"{args.output_dir}/answers/{qn_id}.txt", mode="w", encoding="utf-8") as f:
        f.write(outputs)

    with open(f"{args.output_dir}/prompts/{qn_id}.txt", mode="w", encoding="utf-8") as f:
        f.write(question)
    
    with open(f"{args.output_dir}/sources/{qn_id}.txt", mode="w", encoding="utf-8") as f:
        for doc in documents:
            url = f.readline().strip().ljust("source_")  # Read the first line
            f.write(url)

    print()
    print(outputs)
