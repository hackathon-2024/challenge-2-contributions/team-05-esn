# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import spacy
import chromadb


import pandas as pd
import json
import argparse

parser = argparse.ArgumentParser(description="Process questions and generate answers.")
parser.add_argument("questions_csv", type=str, help="Path to the questions CSV file")
args = parser.parse_args()

chroma_client = chromadb.Client()
collection = chroma_client.create_collection(name="my_collection")

# Load the NLP model for vectorization
nlp = spacy.load("en_core_web_lg")


def vectorize(text):
    """
    Vectorize a piece of text

    :param text: piece of texte to vectorize
    :type text: str
    :return: vectorized text
    :rtype: TBD
    """
    return nlp(text).vector


def store_in_chroma(data):
    """
    Vectorize data and add it to Chroma Collection
    Storage in the Client, not locally

    :param data: _description_
    :type data: str
    """
    for doc in data:
        metadata = {k: v for k, v in doc.items() if k not in ["chunks", "contents"]}
        embeddings = [vectorize(chunk) for chunk in doc["chunks"]]
        # Store the embeddings with metadata in the collection
        collection.add(
            embeddings=embeddings, 
            documents=doc["chunks"], 
            metadatas=[metadata],
        )


# Load the data
with open("data/docs_tagged_chunked.json", mode="r", encoding="utf-8") as f:
    data = json.load(f)


# Store the data in Chroma
store_in_chroma(data)
