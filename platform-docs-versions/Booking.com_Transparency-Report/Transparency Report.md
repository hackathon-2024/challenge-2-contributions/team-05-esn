# Resource URL: https://r-xx.bstatic.com/data/mobile/dsa_transparency_report_bf3fdc24.pdf
Digital Services Act



27 October 2023 - Transparency Report prepared by Booking.com

B.V. under the Digital Services Act

28 August 2023 - 30 September 2023 (inclusive)



About this report



At Booking.com, our mission is to make it easier for everyone to experience the world. We believe that travel can bring out the best

in humanity. Travel promotes a better understanding of different cultures and ways of life. We also believe in and work towards

making travel a force for good in the world - one that enriches people’s lives through a range of experiences, big and small. As a

travel platform, it is at the core of our activities to facilitate travel experiences centred on our customers and underpinned by our

values.



Our long-held values as well as our guidelines and terms and conditions for all users of our platform - travellers and supply

partners - are designed to foster safe and welcoming travel experiences for all. To maintain that environment and ensure the safety

of our travellers and supply partners, we take action upon content that violates the law, our Content Guidelines or Terms and

Conditions.



As required under the Digital Services Act (DSA), this report provides insights into the content moderation activities that we

engaged in during the reporting period, including the volume and nature of content removed from our platform and removal

requests received from public authorities and users.



1

28 August 2023 - 30 September 2023 (inclusive)



Section 1 - EU Member State MAR information from 1 February 2023 up to and including 31 July 2023



Figure 1 - EU Member State MAR Information\*



\*As reflected on Booking.com's DSA webpage, the MAR information is an estimate and is based on the data available to

Booking.com at this time, and the limited guidance in the DSA. This estimate is required to be published under the DSA and should

not be used for any other purpose. The methodologies used to estimate average monthly recipients as defined in the DSA require

significant judgement and design inputs, are subject to data and other limitations, and inherently are subject to statistical variances

and uncertainties. This estimate may be revised as Booking.com refines its approach and in response to the publication of

methodology by the European Commission.

\*\*For ease of reference we have grouped under “Other” all other EU Member States where Booking.com MAR estimates represent

less than 4% of the total.



2

28 August 2023 - 30 September 2023 (inclusive)



Section 2 - Information on number of governmental orders



The section below provides insight into the volume of government data or removal requests we received during the reporting

period, categorised by type of illegal content.



Metric Total number Member States of the European Union



BE DE ES FR HR IT NL PT Other1



Information on number of orders



Number of data requests received 10 1 2 2 1 1 1 2 0 0



Number of illegal content orders

received2

4 0 0 2 0 0 1 0 1 0



Median time to inform the authority of

the receipt of the order

0 (instantly upon

submitting)

0 0 0 0 0 0 0 0 N/A



Median time to give effect to the

order3

10 days N/A N/A N/A N/A N/A N/A N/A N/A N/A



Moderation divided by category



Unsafe and/or illegal products4 14 1 2 4 1 1 2 2 1 0



4 Under the DSA, we are required to categorise orders by the type of illegal content concerned. All orders received concern regulatory

compliance issues which fall under the category of ‘Unsafe and/or illegal products’.



3 Calculated on the basis of the orders processed as at 30 September 2023.



2 Includes illegal listings orders received.



1 Includes all other Member States where no data requests or orders were received.



3

28 August 2023 - 30 September 2023 (inclusive)



Section 3 - Information on number of notices



Metric Total number Notice and Action mechanism (NAM)



NAM total Trusted Flagger



Number of notices received5 15 15 0



Median time to take action on the basis of

the notice6

6.8 days 6.8 days N/A



Number of actions taken on the basis of

the law

0 0 N/A



Number of actions taken on the basis of

the terms and conditions of service

9 9 N/A



Section 4 - Information on own-initiative content moderation



Own-initiative

content

moderation



Total

number

Visibility restriction Monetary

restriction

Provision of the service Account restriction



Removal Other Suspension Termination Suspension Termination



Number of

items

moderated7



Approx. 21

million

N/A N/A N/A N/A N/A N/A N/A



7 Comprises items moderated by our human moderators and our core ML models.



6 Calculated on the basis of the notices processed as at 30 September 2023.



5 Includes notices of illegal listings in the EU only.



4

28 August 2023 - 30 September 2023 (inclusive)



Number of

items detected

solely using

automated

means8



868,637 N/A N/A N/A N/A N/A N/A N/A



Number of

restrictions

imposed9



159,817 151,719 0 0 28 8,069 0 1



Moderation divided by category



Animal

welfare

339 339 0 0 0 0 0 0



Data

protection and

privacy

violations



64,49410 64,494 0 0 0 0 0 0



Illegal or

harmful

speech



357 357 0 0 0 0 0 0



Intellectual

property

269 269 0 0 0 0 0 0



10 Includes instances of travellers or partners erroneously sharing personal data about other persons, including but not limited to sensitive or

special-category personal data.



9 Comprises restrictions on content applied in the EU, and restrictions on EU listings and EU accounts related to the provision of information that

is illegal or incompatible with our terms and conditions.



8 Comprises items detected solely using our core ML models.



5

28 August 2023 - 30 September 2023 (inclusive)



infringements



Negative

effects on civic

discourse or

elections



0 0 0 0 0 0 0 0



Non-consensu

al behaviour

0 0 0 0 0 0 0 0



Pornography

or sexualised

content11



155 155 0 0 1 0 0 1



Protection of

minors

0 0 0 0 0 0 0 0



Risk for public

security

0 0 0 0 0 0 0 0



Scams and/or

fraud12

39,617 31,548 0 0 0 8,069 0 0



Unsafe and/or

illegal

products



61 34 0 0 27 0 0 0



Violence 4 4 0 0 0 0 0 0



Scope of 54,520 54,520 0 0 0 0 0 0



12 Includes restrictions imposed on inauthentic EU listings and inauthentic user reviews.



11 Comprises content uploaded by travellers or partners which violates our content guidelines as it contains content of a sexual nature.



6

28 August 2023 - 30 September 2023 (inclusive)



platform

service



Section 5 - Suspensions imposed on repeated offenders



Metric Total number



Number of suspensions enacted for the provision of manifestly

illegal content

0



Number of suspensions enacted for the provision of manifestly

unfounded notices

0



Number of suspensions enacted for the provision of manifestly

unfounded complaints

0



Section 6 - Out of court dispute settlement bodies and internal complaints mechanism



Metric Number



Out-of-court dispute settlement bodies



Number of decisions submitted to out-of-court dispute

settlement bodies

0



Internal complaints mechanism



Number of complaints submitted to the internal-complaints

mechanism

3192



7

28 August 2023 - 30 September 2023 (inclusive)



Procedural complaints 0



Substantive complaints on the illegality or incompatibility 3192



Number of decision reversals 635



Section 7 - Use of automated means for content moderation and human resources



Metric Total number Internal complaints Own-initiative Notice and

Action

mechanism

(NAM)



Languages of the MS of

the EU



NAM

total

Trusted

flagger



Accuracy rate

of the items

processed

solely by

automated

means13



99.122% N/A 99.122% N/A N/A English - 99.7

Estonian - 98.9

Finnish - 99.2

French - 99.9

German - 99.9

Greek - 99.7

Hungarian - 97.8

Irish - N/A

Italian - 99.5

Latvian - 98.4

Lithuanian - 98.9

Maltese - N/A



13 The accuracy rate is calculated for our core ML models only, and excludes our ancillary ML models.



8

28 August 2023 - 30 September 2023 (inclusive)



Polish - 99.9

Portuguese - 99.7

Romanian - 94.7

Slovak - 99.7

Slovenian - N/A

Spanish - 99.9

Swedish - 99.7



Accuracy rate

of the items

processed

partly by

automated

means



N/A N/A N/A N/A N/A N/A



Error rate of

the

automated

means

applied14



0.878% N/A 0.878% N/A N/A N/A



Human resources dedicated to content moderation



Number of

internal

moderators

employed by



22 7 22 2 2 English 22

Dutch 3

German 1

Italian 3



14 The error rate is calculated for our core ML models only, and excludes our ancillary ML models.



9

28 August 2023 - 30 September 2023 (inclusive)



the provider15 Spanish 5

French 2

Greek 1



Number of

external

moderators

contracted by

the provider



13 0 13 0 0 English 13

Romanian 13

German 2

French 2

Spanish 1



Section 8 - Statement on content moderation



Information about content moderation



Own initiative moderation



Summary of the content moderation engaged in at our own

initiative

Booking.com aims to reflect the most up-to-date reviews from

our travellers. That’s why we have a fast turnaround on our

content moderation, using both moderation by automated

Machine Learning (ML) models and manual review.



Meaningful and comprehensible information regarding the

applied detection method

Our core ML algorithms swiftly review various types of content,

such as guest reviews, partner responses, photos and more.

Most content is approved within seconds and goes live on



15 Comprises resources fully dedicated to content moderation activities and does not include all resources in our Fraud and Trust and Safety

teams whose scope is larger than content moderation.



10

28 August 2023 - 30 September 2023 (inclusive)



Booking.com. Anything not approved by our core ML algorithms

is sent to our moderators for further review when potential

guideline violations are detected. Approved content is

accessible on our platform and apps, while policy-violating

content won't be published. Our decision can be appealed or

the content can be resubmitted.



Use made of automated means



Summary of the use made of automated means for the purpose

of content moderation

Booking.com maintains two core ML models in 43 different

languages to detect inappropriate content. We handle the bulk

of content moderated through automation, one core model for

text content and the other core model for images.



In addition to our core ML models, we utilise human and

machine intelligence to monitor offerings on our platform and

to safeguard its integrity against fraudulent actors. For this

purpose, we maintain four ancillary ML models: three for

inauthentic listings and one for inauthentic reviews.



Qualitative description of the automated means The core ML models Booking.com applies are content classifier

models which are tuned to identify context. The ancillary ML

models use numerous different data points and fraud indicators

to detect inauthentic listings and inauthentic reviews.



Specification of the precise purposes to apply automated

means

The core ML models are designed to detect both illegal content

and content that violates Booking’s content policies. Illegal and

violating content identified by the core ML models is sent to our

content moderators for human review. Content moderators will

make the final decision. The ancillary ML models are designed



11

28 August 2023 - 30 September 2023 (inclusive)



and used to detect inauthentic listings and inauthentic reviews

at scale.



Safeguards applied to the use of automated means We perform constant random sampling of the automatically

approved items and send them to moderators to ensure that

the quality of ML automated content approvals is within the

acceptable range.



Specific elements of the human resources dedicated to content moderation



Qualifications of the human resources dedicated to content

moderation

1 senior manager, 3 team leads, 2 project managers, 4

associates, 22 content moderators, 13 vendor content

moderators.



Training given to human resources dedicated to content

moderation

When new policies are launched or a new content moderator is

onboarded, training decks and videos are provided to introduce

the new content policies. Content moderators spend on

average approximately 6 hours monthly receiving training,

reviewing content guidelines and policy clarifications, reviewing

their errors and asking questions. Frequently asked questions

are compiled, and grey areas are clarified on a regular basis.



Support given to human resources dedicated to content

moderation

Internal employees can reach out to the employee assistance

program that provides counselling services, practical

information and digital content to support employees’ mental,

physical, social and financial well-being. All moderators have

followed a course about dealing with distressing content,

designed to increase awareness, learn about prevention of

potential issues and to understand what support is available.

Our external partner contracted for moderation has a consistent

approach towards employee safety utilising SGS audits and



12

28 August 2023 - 30 September 2023 (inclusive)



participating with renown partners.



13