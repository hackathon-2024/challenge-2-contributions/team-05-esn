# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import spacy
import chromadb


def get_docs():
    """
    Get all docs (markdown files) provided

    :return: list of document
    :rtype: list
    """
    platforms = {}

    dirs = [
        dir
        for dir in os.listdir("platform-docs-versions")
        if os.path.isdir(os.path.join("platform-docs-versions", dir))
        and not dir.startswith(".")
    ]

    for dir in dirs:
        parts = dir.split("_")
        platform = parts[0]
        if platform not in platforms:
            platforms[platform] = {}
        tool = parts[1].replace("-", " ")
        platforms[platform][tool] = []

        files = [
            file
            for file in os.listdir(os.path.join("platform-docs-versions", dir))
            if file.endswith(".md")
        ]

        platforms[platform][tool].extend(files)

    print(platforms)

    docs = []

    for platform in platforms:
        for tool in platforms[platform]:
            for file in platforms[platform][tool]:
                print(f"{platform} -> {tool} -> {file}")
                doc = {
                    "platform": platform,
                    "tool": tool,
                    "file": file,
                    "contents": "",
                }
                with open(
                    os.path.join(
                        "platform-docs-versions",
                        platform + "_" + tool.replace(" ", "-"),
                        file,
                    ),
                    mode="r",
                    encoding="utf-8",  # Specify the encoding as 'utf-8'
                ) as f:
                    doc["contents"] = f.read()

                # take the first line of the contents, if it contains http, there's a link which should go to doc['source_url']
                first_line = doc["contents"].split("\n")[0]
                if "http" in first_line:
                    # extract the url
                    doc["source_url"] = first_line.split("http")[1]
                    doc["source_url"] = "http" + doc["source_url"]

                docs.append(doc)

    return docs


def get_nlp():
    # Load the large English NLP model for vectorization
    # /!\ Some documentation is in french, how does it impact processing ?
    spacy.prefer_gpu()
    return spacy.load("en_core_web_lg")


def vectorize(texts, nlp=None):
    """
    Vectorize a list of texts using the given spaCy NLP model
    texts: List[str]: A list of texts to vectorize
    """
    # Process the texts as a batch and return their vectors
    docs = list(nlp.pipe(texts))
    return [doc.vector.tolist() for doc in docs]


def get_collection():
    chroma_client = chromadb.PersistentClient()
    # chroma_client = chromadb.Client()
    try:
        return chroma_client.create_collection(name="my_collection")
    except Exception as e:
        print(e)
        print("-> Collection already exists, returning existing collection")
        return chroma_client.get_collection(name="my_collection")


def retrieve_texts(collection, vectorized_queries):
    retrieved_texts = []
    for query_vector in vectorized_queries:
        # Query the collection for similar texts
        results = collection.query(query_texts=[query_vector], n_results=5)
        retrieved_texts.append(results)
    return retrieved_texts
