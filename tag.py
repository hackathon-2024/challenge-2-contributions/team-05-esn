# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import spacy
import src.utils as utils
import json
import argparse

parser = argparse.ArgumentParser(description="Process questions and generate answers.")
parser.add_argument("questions_csv", type=str, help="Path to the questions CSV file")
args = parser.parse_args()

# Load the NER model
nlp = spacy.load("en_core_web_lg")
nlp.max_length = 1500000

# Define the keywords for matching
keywords = {
    "API Usage": ["curl", "API", "query", "endpoint", "request", "API token"],
    "API Documentation Reference": ["documentation", "API doc", "docs"],
    "Platform-Specific Inquiry": [
        "Linkedin",
        "Pinterest",
        "Facebook",
        "Twitter",
        "TikTok",
        "Snapchat",
    ],
    "Data Request Statistics": [
        "data request",
        "statistics",
        "reports",
        "government data requests",
    ],
    "Content Moderation": [
        "content moderation",
        "moderation carried out",
        "moderation process",
    ],
    "Legal or Regulatory Inquiry": [
        "legal representative",
        "DSA",
        "representant légal",
    ],
    "Technical Query or Code Snippet": [
        "bash code",
        "snippet",
        "script",
        "coding expert",
    ],
    "Platform Transparency and Operations": [
        "transparency center",
        "transparency report",
        "operations",
    ],
    "Data Privacy": [
        "privacy",
        "personal data",
        "data protection",
        "GDPR",
    ],
    "Security Concerns": [
        "security",
        "vulnerability",
        "breach",
        "cybersecurity",
    ],
}


def label_text(text, keywords):
    # Convert text to lowercase for case-insensitive matching
    lower_text = text.lower()

    # Perform keyword matching
    matched_keywords = []
    for category, keys in keywords.items():
        for keyword in keys:
            if keyword.lower() in lower_text:
                matched_keywords.append(f"{category} - {keyword}")

    # Perform NER
    doc = nlp(text)
    for ent in doc.ents:
        matched_keywords.append(ent.label_ + " - " + ent.text)

    return matched_keywords


docs = utils.get_docs()

for counter, doc in enumerate(docs, start=1):
    label = label_text(doc["contents"], keywords)
    doc["label"] = label
    print(f"File: {doc['file']}\nLabel: {label}\n")
    print(f"Processed {counter}/{len(docs)} documents")

# create a data dir if it doesn't exist
os.makedirs("data", exist_ok=True)

# write the docs to a file
with open("data/docs_tagged.json", "w") as f:
    f.write(json.dumps(docs, indent=4))
