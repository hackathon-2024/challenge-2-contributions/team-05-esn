# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import pandas as pd
from src.utils import vectorize, get_collection, get_nlp, retrieve_texts
import argparse

parser = argparse.ArgumentParser(description="Process questions and generate answers.")
parser.add_argument("questions_csv", type=str, help="Path to the questions CSV file")
args = parser.parse_args()
collection = get_collection()


df = pd.read_csv(args.questions_csv)

# Extract the questions
nlp = get_nlp()
questions = df["question"].tolist()
vectorized_queries = [vectorize(question, nlp) for question in questions]
# Retrieve texts for each query
retrieved_texts_for_queries = retrieve_texts(collection, vectorized_queries)

print(retrieved_texts_for_queries)
