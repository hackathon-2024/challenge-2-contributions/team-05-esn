#!/bin/bash

# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

if [ "$#" -ne 2 ]; then
    echo "Usage: ./run.sh path/to/questions.csv path/to/output"
    exit 1
fi

QUESTIONS_CSV=$1
OUTPUT_DIR=$2

# Environment setup
# Activate Python environment
source .venv/bin/activate

# Install requirements without using cache
pip install --no-cache-dir -r requirements.txt

python tag.py --questions-csv $QUESTIONS_CSV
python chunk_text.py --questions-csv $QUESTIONS_CSV
python embed.py --questions-csv $QUESTIONS_CSV
python chat.py --questions-csv $QUESTIONS_CSV --output-dir $OUTPUT_DIR
