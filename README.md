<!--
SPDX-FileCopyrightText: 2024 ESN
SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: EUPL-1.2
-->

# Team Name : ESN

## Licence / License
	
:fr: Ce code a été produit pour le Challenge 2 du Hackathon 2024 co-organisé par le PEReN et la Commission Européenne : DSA RAG Race. Il est délivré comme récupéré à la fin de l'événement et peut donc ne pas être exécutable.
Sauf mention contraire, le code source est placé sous licence publique de l'Union Européenne, version 1.2 (EUPL-1.2).
Les données des plateformes présentes sur ce projet sont la propriété des plateformes et ne sont fournies qu'à titre d'illustration pour le bon fonctionnement du projet. Elles ne seront en aucun cas mises à jour. Les données complètes peuvent être trouvées vers ce lien : https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

:gb: This code was developed for Challenge 2 of the Hackathon 2024 co-organized by the PEReN and the European Commission: DSA RAG Race. It is released as retrieved at the end of the event and may therefore not be executable.
Unless otherwise specified, the source code is licensed under the European Union Public License, version 1.2 (EUPL-1.2).
The data of platforms contained in this project are the property of the platforms and are provided for illustrative purposes only. They will not be updated under any circumstances. The complete data can be found at the following link: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions


## Introduction

This project utilizes advanced NLP techniques and models to process, embed, search, and interact with textual data. Leveraging libraries such as SpaCy and HuggingFace's transformers, it provides functionalities for chunking text, embedding data, querying through a database, and interacting via a chat interface.


## Libraries used in the project

- [SpaCy for NER](https://spacy.io/api/entityrecognizer) 
- [ChromaDB](https://docs.trychroma.com/)  
- [transformers](https://huggingface.co/docs/transformers/index)  
- [pandas](https://pandas.pydata.org/docs/)

## Files

1. **`tag.py`**: Uses SpaCy for Named Entity Recognition (NER) and categorizes or tags text data based on predefined keywords.
2. **`chunk_text.py`**: Contains functions to chunk text using the SpaCy NLP library, useful for breaking down large text into smaller, manageable chunks.
3. **`embed.py`**: Involves embedding textual data for storage or further processing, using vectorization techniques and possibly interacting with a Chroma collection.
4. **`search.py`**: Similar to `embed.py`, it uses vectorization and retrieval functions for searching through a database or collection of texts based on queries from a CSV file.
5. **`chat.py`**: Utilizes HuggingFace's transformers to load Mixtral-8x7B-Instruct-v0.1 and processes a set of questions loaded from a CSV file. through this model.

The `run.sh` bash file runs through these in the necessary order and produces the outputs in an output directory with 3 subdirectories for answers, the full augmented prompted run to retrieve these answers, and the urls of sources needed. 

