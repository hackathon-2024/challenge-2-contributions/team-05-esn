# SPDX-FileCopyrightText: 2024 ESN
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import spacy
import json
import warnings
import argparse

parser = argparse.ArgumentParser(description="Process questions and generate answers.")
parser.add_argument("questions_csv", type=str, help="Path to the questions CSV file")
args = parser.parse_args()
# Suppress warning regarding POS tagger missing (not needed for chunking)
warnings.filterwarnings("ignore", message="\\[W108\\]")

# Load SpaCy model with only the tokenizer and sentencizer for efficiency
nlp = spacy.load("en_core_web_lg", disable=["tagger", "parser", "ner"])
nlp.add_pipe("sentencizer")


def chunk_text(text, chunk_size=200, max_sentences=3):
    """
    Splits text into chunks based on chunk size or maximum sentence count.
    """
    chunks = []
    paragraphs = text.split("\n")

    for paragraph in paragraphs:
        if not paragraph.strip():  # Skip empty paragraphs
            continue

        sentence_group = ""
        sentence_count = 0
        for sentence in nlp(paragraph).sents:
            sentence_text = str(sentence)

            # Si on dépasse la taille max imposée du chunk, on enregistre ce chunk
            # et on commence un nouveau chunk
            if (
                len(sentence_group) + len(sentence_text) > chunk_size
                or sentence_count == max_sentences
            ):
                chunks.append(sentence_group.strip())
                sentence_group = sentence_text  # Start a new chunk
                sentence_count = 1  # Reset sentence count for the new chunk
            
            # Sinon on forme un nouveau chunk
            else:
                sentence_group += (
                    " " + sentence_text if sentence_group else sentence_text
                )
                sentence_count += 1

        # PErmet de gérer la fin des données
        if sentence_group.strip():  # Add any remaining text as a chunk
            chunks.append(sentence_group.strip())

    return chunks


def process_documents(input_path, output_path):
    """
    Processes documents by chunking text and updates the JSON file.
    """
    # Read the data once
    with open(input_path, mode="r", encoding="utf-8") as file:
        data = json.load(file)

    # Process each document
    for i, doc in enumerate(data, start=1):
        chunks = chunk_text(doc["contents"])
        doc["chunks"] = chunks  # Update document with chunks
        print(f"\rProcessed {i}/{len(data)} documents", end="")

    # Write the updated data once
    with open(output_path, "w", encoding="utf-8") as file:
        json.dump(data, file, indent=4)


# File paths
input_file = "data/docs_tagged.json"
output_file = "data/docs_tagged_chunked.json"

# Process documents
process_documents(input_file, output_file)
